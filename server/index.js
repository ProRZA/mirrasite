const express = require('express');
const path = require('path');
const mysql = require('mysql');
const multer = require('multer');
const configBase = require('./config');
const webpack = require('webpack');
const webpackDevMiddleware = require('webpack-dev-middleware');
const webpackHotMiddleware = require('webpack-hot-middleware');

const db = mysql.createConnection(configBase.dataBase[configBase.mode]);

module.exports = db;
db.connect();

const app = express();
const port = process.env.PORT || 3000;
const DIST_DIR = path.join(__dirname, '/../dist');
const HTML_FILE = path.join(DIST_DIR, 'index.html');
const isDev = process.env.NODE_ENV === 'development';


    const config = require('../webpack.config');
    const compiler = webpack(config);
    app.use(
        webpackDevMiddleware(compiler, {
            hot: true,
            noInfo: true,
            publicPath: config.output.publicPath
        })
    );

    app.use(webpackHotMiddleware(compiler));

app.use(express.static(DIST_DIR));

const storage = multer.diskStorage({
    destination: path.join(DIST_DIR, '/image'),
    filename: (req, file, cb) => {
        let nameFile = file.originalname.match(/([^.])+/i);
        cb(null,nameFile[0]+'-'+Date.now()+path.extname(file.originalname));
    }
});

const upload = multer({
    storage: storage,
    limits: {fileSize: 5*1024*1024},
});

app.use(express.urlencoded());
app.use(express.json());

app.get('*', (req, res) => {
    res.sendFile(HTML_FILE);
});

// Загрузка изображений на сервер
app.post('/api/image/save', upload.single('photo'), (req, res) => {
    if (req.file){
        res.json({'pathToFile': res.req.file.filename})
    } else {
        res.json({'pathToFile': ''});
    }
});

app.listen(port, function () {
    console.log('App listening on port: ' + port);
});

// ---------------Фильтры------------------///
// Чтение всех фильтров
app.post('/api/filters/get', (req, res) => {
    db.query("SELECT * FROM `filters`",(err, result) => {
        if (err) throw err;
        res.status(200).json(result);
    });
});

// Добавление фильтров
app.post('/api/filter/edit/add', (req, res) => {
    const filter_name = req.body.filter_name;
    const value = req.body.value;
   db.query("INSERT INTO `filters` (`value`, `filter_name`) VALUES (?,?)",[value, filter_name], (err, result) => {
       if (err) throw err;
       res.status(200).json({id: result.insertId, value, filter_name});
    });
});

// редактирование фильтров
app.patch('/api/filter/edit/edit', (req, res) => {
    const id = req.body.id;
    const value = req.body.value;
    db.query("UPDATE `filters` SET `value` = ? WHERE id = ?",[value, id], (err, result) => {
        if (err) throw err;
        res.status(200).json('OK');
    });
});

// Удаление фильтров
app.delete('/api/filter/edit/delete', (req, res) => {
    const id = req.body.id;
    db.query("DELETE FROM `filters` WHERE id = ?",[id], (err, result) => {
        if (err) throw err;
        res.status(200).json('OK');
    });
});
// ---------------Фильтры------------------///
// ---------------Товары------------------///
// добавление товара
app.post('/api/goods/edit/add', (req, res) => {
    const brand = req.body.brand;
    const name = req.body.name;
    const description = req.body.description;
    const price = req.body.price;
    const filter = req.body.filter;
    const image = req.body.image;
    const query = "INSERT INTO `goods` (`brand`, `name`, `description`, `price`, `image`) VALUES (?,?,?,?,?)";
    db.query(query, [brand, name, description, price, image], (err, result) => {
        if (err) throw err;
        const id_goods = result.insertId;
        const filters = filter.map(item => [id_goods, item]);
        db.query("INSERT INTO `filter_goods` (`id_goods`, `id_filters`) VALUES ?", [filters], (err, result) => {
           if (err) throw err;
           res.status(200).json('OK');
        });
    });
});

app.post('/api/goods/get', (req, res) => {
    const data = req.body;
    let filters = '';
    let query = '';
    if (data.length > 0) {
       filters = data.reduce((acc, item) => {
           return acc + item + ',';
       }, '').slice(0,-1);
        query = 'SELECT `goods`.`id`, `name`, `price`, `image` FROM `goods`' +
            ' JOIN `filter_goods` ON `goods`.`id` = `filter_goods`.`id_goods`' +
            ' WHERE `filter_goods`.`id_filters` IN (?) ORDER BY `id`';
    } else {
        query = 'SELECT `id`, `name`, `price`, `image` FROM `goods` ORDER BY `id`';
    }

    db.query(query, [filters],(err, result) => {
        if (err) throw err;
        res.status(200).json(result);
    });
});

app.post('/api/goods/view/single', (req, res) => {
   const id = req.body.id_goods;
   const query = 'SELECT `goods`.*, GROUP_CONCAT(`filters`.`value`) as `value` FROM `goods`' +
       'JOIN `filter_goods` ON `goods`.`id` = `filter_goods`.`id_goods`' +
       'JOIN `filters` ON `filter_goods`.`id_filters` = `filters`.`id`' +
       'WHERE `filter_goods`.`id_goods` = ? GROUP BY `goods`.`id`';
   db.query(query, [id], (err, result) => {
       if (err) throw err;
       res.status(200).json(result);
   });
});

app.post('/api/goods/checkExistsGoods', (req, res) => {
    const nameGoods = req.body.nameGoods;
    const query = "SELECT `goods`.`name` FROM `goods` WHERE `name` LIKE '"+nameGoods+"%'";
    db.query(query, [nameGoods], (err, result) => {
        if (err) throw err;
        res.status(200).json(result);
    });
});
