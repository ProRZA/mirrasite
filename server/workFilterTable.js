const getQuerySelect = (table) => {
    let query = '';
    switch (table) {
        case 'ages': query = "SELECT * FROM `ages` ORDER BY ID"; break;
        case 'illness': query = "SELECT * FROM `illness` ORDER BY ID"; break;
        case 'skin': query = "SELECT * FROM `filters` ORDER BY ID"; break;
    }
    return query;
};

const getQueryInsert = (table) => {
    let query = '';
    switch (table) {
        case 'ages': query = "INSERT INTO `ages` (`value`) VALUES (?)"; break;
        case 'illness': query = "INSERT INTO `illness` (`name`) VALUES (?)"; break;
        case 'skin': query = "INSERT INTO `filters` (`name`) VALUES (?)"; break;
    }
    return query;
};

const getQueryDelete = (table) => {
    let query = '';
    switch (table) {
        case 'ages': query = "DELETE FROM `ages` WHERE `id` = ?"; break;
        case 'illness': query = "DELETE FROM `illness` WHERE `id` = ?"; break;
        case 'skin': query = "DELETE FROM `filters` WHERE `id` = ?"; break;
    }
    return query;
};

exports.getQuerySelect = getQuerySelect;
exports.getQueryInsert = getQueryInsert;
exports.getQueryDelete = getQueryDelete;
