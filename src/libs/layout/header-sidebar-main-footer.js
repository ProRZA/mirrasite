import React, { Component } from "react";
import PropTypes from "prop-types";
import {connect} from "react-redux";
import { withRouter } from 'react-router-dom';

import Header from "../components/HeaderComponent";
import MainMenu from "../../pages-site/components/MainMenuComponent";
import Breadcrumbs from "../components/BreadcrumbsComponent";
import Footer from "../components/FooterComponent";

const menuData = [
    {
        title: 'Услуги',
        drop: true,
        links: [
            {
                title: 'Анализы',
                drop: false,
                links: ""
            },
            {
                title: 'Диагностика',
                drop: false,
                links: ""
            },
        ]
    },
    {
        title: 'Косметология',
        drop: true,
        links: [
            {
                title: 'Чистка',
                drop: false,
                links: ""
            },
            {
                title: 'Пилинг',
                drop: false,
                links: ""
            },
            {
                title: 'Ботулинотерапия',
                drop: false,
                links: ""
            },
            {
                title: 'Биоревитализация',
                drop: false,
                links: ""
            },
            {
                title: 'Мезотерапия',
                drop: false,
                links: ""
            },
            {
                title: 'Плазмотерапия',
                drop: false,
                links: ""
            },
            {
                title: 'Биорепарация',
                drop: false,
                links: ""
            },
            {
                title: 'Контурная пластика',
                drop: false,
                links: ""
            },
            {
                title: 'Нити',
                drop: false,
                links: ""
            },
            {
                title: 'Удаление новообразований',
                drop: false,
                links: ""
            },
            {
                title: 'Эпиляция',
                drop: false,
                links: ""
            }
        ]
    },
    {
        title: 'Купить косметику',
        drop: false,
        links: "/shop"
    },
    {
        title: 'Статьи',
        drop: false,
        links: ""
    }
];

export class HeaderSidebarMainFooter extends Component {
    constructor(props) {
        super(props);
    }

    static get propTypes() {
        return {
            content: PropTypes.shape(),
            sidebar: PropTypes.shape(),
            showMainMenu: PropTypes.bool,
        }
    }

    static get defaultProps() {
        return {
            content: null,
            sidebar: null,
            showMainMenu: true,
        }
    }

    render() {
        const {
            content,
            sidebar,
            showMainMenu,
        } = this.props;

        const url = this.props.match.path.split('/');
        const breadcrumbData = url.map(item => {
            if (item.length === 0) {
                return {
                    title: 'Home',
                    target: '/',
                }
            }
            return {
                title: item,
                target: item,
            }
        });
        const currentPage = breadcrumbData.length === 1 ? 'Дом' : breadcrumbData[breadcrumbData.length-1].title;

        return (
            <div className="layout">
                <div className="wrapper-content">
                    <Header/>
                    {showMainMenu && (
                        <MainMenu
                            options={menuData}
                        />
                    )}
                    <Breadcrumbs
                        links={breadcrumbData.reverse().slice(1).reverse()}
                        currentPage={currentPage}
                    />
                    <div className="content container_">
                        {sidebar}
                        {content}
                    </div>
                </div>
                <div className="tag"></div>
                <Footer/>
            </div>
        )
    }
}

// export default connect((state) => {
//     const {
//
//     } = state;
//     return {
//
//     };
// })(HeaderSidebarMainFooter);

 export default withRouter(HeaderSidebarMainFooter);
