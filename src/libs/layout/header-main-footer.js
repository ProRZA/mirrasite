import React, { Component } from "react";
import PropTypes from "prop-types";

import Header from "../components/HeaderComponent";
import Footer from "../components/FooterComponent";

class HeaderMainFooter extends Component {
    constructor(props) {
        super(props);
    }

    static get propTypes() {
        return {
            content: PropTypes.shape(),
            showMainMenu: PropTypes.bool,
        }
    }

    static get defaultProps() {
        return {
            content: null,
            showMainMenu: true,
        }
    }

    render() {
        const { content, showMainMenu } = this.props;

        return (
            <div className="layout">
                <div className="wrapper-content">
                    <Header
                        showMainMenu={showMainMenu}
                    />
                    {content}
                </div>
                <div className="tag"></div>
                <Footer/>
            </div>
        )
    }
}

export default HeaderMainFooter
