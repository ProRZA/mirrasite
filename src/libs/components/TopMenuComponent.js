import React, { Component } from "react";
import PropTypes from 'prop-types';

class TopMenu extends Component {
    constructor(props) {
        super(props);
    }

    static get propTypes() {
        return {

        }
    }

    static get defaultProps() {
        return {

        }
    }

    render() {
        return (
            <nav>
                <ul className="top-menu">
                    <li className="top-menu-link">Home</li>
                    <li className="top-menu-link">Услуги
                        <ul className="drop">
                            <div>
                                <li>Анализы</li>
                                <li>Диагностика</li>
                            </div>
                        </ul>
                    </li>
                    <li className="top-menu-link">Skills
                        <ul className="drop">
                            <div>
                                <li>scss</li>
                                <li>jquery</li>
                                <li>html</li>
                            </div>
                        </ul>
                    </li>
                    <li className="top-menu-link">Contact</li>
                    <div className="top-menu-marker"></div>
                </ul>
            </nav>
        )
    }
}

export default TopMenu;
