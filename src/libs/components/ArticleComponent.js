import React, { Component } from "react";
import PropTypes from 'prop-types';

class Article extends Component {
    constructor(props) {
        super(props);
    }

    static get propTypes() {
        return {
            title: PropTypes.string,
            text: PropTypes.string.isRequired,
            blockLinks: PropTypes.shape(),
        }
    }

    static get defaultProps() {
        return {
            title: '',
            text: '',
            blockLinks: {},
        }
    }

    render() {
        const { title, text, blockLinks } = this.props;
        return (
            <>
                <div className="background"></div>
                <div className="service__caption">{title}</div>
                <div className="service__content">
                    <div className="service__content_block heightContent">
                        <div className="background"></div>
                        <div className="service__content-text">{text}</div>
                    </div>
                    {blockLinks}
                </div>
                <div className="tag"></div>
            </>
        )
    }
}

export default Article;
