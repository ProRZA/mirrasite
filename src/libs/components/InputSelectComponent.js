import React, { Component } from "react";
import PropTypes from 'prop-types';

import Input from "./InputComponent";

export class InputSelect extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showItems: false,
            style: {},
            hoverItem: false,
        };
        this.ref = React.createRef();
    }

    static get propTypes() {
        return {
            typeInput: PropTypes.string,
            options: PropTypes.arrayOf(
                PropTypes.oneOfType([
                    PropTypes.string,
                    PropTypes.number,
                ]),
            ),
            modified: PropTypes.string,
            modifiedContainer: PropTypes.string,
            modifiedLabel: PropTypes.string,
            placeholder: PropTypes.string,
            labelText: PropTypes.string,
            setValue: PropTypes.func,
            value: PropTypes.oneOfType([
                PropTypes.string,
                PropTypes.number,
            ]),
            icon: PropTypes.string,
            onValidate: PropTypes.func,
            onSelectItem: PropTypes.func,
        }
    }

    static get defaultProps() {
        return {
            typeInput: 'text',
            options: [],
            modified: '',
            modifiedContainer: '',
            modifiedLabel: '',
            placeholder: '',
            labelText: '',
            setValue: () => {},
            value: '',
            icon: '',
            onValidate: () => {},
            onSelectItem: () => {},
        }
    }

    shouldComponentUpdate(nextProps, nextState) {
        if (nextProps.value !== this.props.value && nextProps.value.length > 2) {
            const pos = this.ref.current.getBoundingClientRect();
            const style = {
                width: pos.width,
            };
            this.setState({ showItems: true, style });
        }

        if ((nextProps.value !== this.props.value && nextProps.value.length < 3) ||
            (!nextState.showItems && nextState.showItems !== this.state.showItems)) {
            this.setState({ showItems: false, hoverItem: false });
        }
        return true;
    }

    render() {
        const {
            labelText,
            modified,
            placeholder,
            modifiedContainer,
            modifiedLabel,
            icon,
            onValidate,
            typeInput,
            setValue,
            value,
            options,
        } = this.props;
        const { showItems, style, hoverItem } = this.state;

        return (
            <div ref={this.ref} style={{position: "relative"}}>
                <Input
                    typeInput={typeInput}
                    labelText={labelText}
                    modified={modified}
                    placeholder={placeholder}
                    icon={icon}
                    modifiedContainer={modifiedContainer}
                    modifiedLabel={modifiedLabel}
                    value={value}
                    setValue={setValue}
                    onValidate={onValidate}
                    onBlur={() => {
                        if (!hoverItem) this.setState({ showItems: false })
                    }}
                />
                <div
                    className={showItems && options.length > 0 ? 'input-select' : 'display-none'}
                    style={style}
                >
                    {showItems && options.length > 0 && options.map((item, index) => {
                        return (
                            <div
                                key={index}
                                className="input-select-item"
                                onClick={() => {
                                    this.setState(prevState => (
                                        { showItems: false }
                                    ), setValue(item));
                                }}
                                onMouseEnter={() => this.setState({ hoverItem: true })}
                                onMouseLeave={() => this.setState({ hoverItem: false })}
                            >
                                {item}
                            </div>
                        )
                    })}
                </div>
            </div>
        )
    }
}

export default InputSelect;
