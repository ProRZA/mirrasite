import React, { Component } from "react";
import PropTypes from 'prop-types';

import Input from "./InputComponent";
import Button from "./ButtonComponent";

class EditText extends Component {
    constructor(props) {
        super(props);
        this.state = {
            value: props.value,
            heightInput: null,
        };
        this.ref = React.createRef();
    }

    static get propTypes() {
        return {
            placeholder: PropTypes.string,
            buttons: PropTypes.arrayOf(
                PropTypes.shape({
                    icon: PropTypes.string,
                    click: PropTypes.func,
                }).isRequired,
            ),
            params: PropTypes.oneOfType([
                PropTypes.string,
                PropTypes.number,
            ]),
            value: PropTypes.string,
            clearAfterSend: PropTypes.bool,
            label: PropTypes.string,
            modifiedLabel: PropTypes.string,
            modifiedInput: PropTypes.string,
        }
    }

    static get defaultProps() {
        return {
            placeholder: '',
            params: '',
            value: '',
            clearAfterSend: false,
            label: '',
            modifiedLabel: '',
            modifiedInput: '',
        }
    }

    componentDidMount() {
        if (!this.state.heightInput && this.ref.current) {
            this.setState( { heightInput: this.ref.current.getBoundingClientRect().height });
        }
    }

    render() {
        const {
            buttons,
            placeholder,
            params,
            clearAfterSend,
            label,
            modifiedLabel,
            modifiedInput,
        } = this.props;
        const { value, heightInput } = this.state;

        return (
            <div
                ref={this.ref}
                className="in-row"
            >
                <Input
                    modified={modifiedInput}
                    labelText={label}
                    modifiedLabel={modifiedLabel}
                    placeholder={placeholder}
                    value={value}
                    setValue={v => this.setState({value: v})}
                />
                {buttons.map((item, index) => (
                    <Button
                        key={index}
                        styles={{height: heightInput, width: heightInput}}
                        modified={`${item.modified}`}
                        icon={item.icon}
                        click={() => {
                            item.click(value, params);
                            if (clearAfterSend) this.setState({value: ''});
                        }}
                    />
                ))}
            </div>
        )
    }
}

export default EditText;
