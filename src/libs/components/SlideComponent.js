import React, { Component } from "react";
import PropTypes from 'prop-types';

export class Header extends Component {
    constructor(props) {
        super(props);
    }

    static get propTypes() {
        return {
            prefix: PropTypes.string,
            currency: PropTypes.string,
            boundaryPrice: PropTypes.array,
        }
    }

    static get defaultProps() {
        return {
            prefix: '',
            currency: '₽',
            boundaryPrice: [],
        }
    }

    render() {
        const {  } = this.props;
        const {  } = this.state;
        return (
            <div className={`${prefix}`}>
                <div className={`${prefix}__bar`}>
                    <div className={`${prefix}__bar`}>
                        <div className="slider__bar_tug" regard="min"></div>
                        <div className="slider__bar_tug" regard="max"></div>
                        <div className="slider__bar_line"></div>
                    </div>
                    <div className="slider__bar_price">
                        <input type="text" className="minmax" regard="min"/>
                        <input type="text" className="minmax" regard="max"/>
                    </div>
                </div>
            </div>
        )
    }
}
