import React, { Component } from "react";
import PropTypes from 'prop-types';

class ModalForm extends React.PureComponent {
    constructor(props) {
        super(props);
    }

    static get propTypes() {
        return {
            content: PropTypes.shape(),
            closeForm: PropTypes.func,
        }
    }

    static get defaultProps() {
        return {
            content: {},
            closeForm: () => {},
        }
    }

    render() {
        const { content, closeForm } = this.props;

        return (
            <div className="modalForm">
                <div
                    className="emersion"
                >
                    <div className="caption">
                        <div
                            className="closeButton"
                            onClick={closeForm}
                        >
                            <i className="fas fa-times-circle"></i>
                        </div>
                    </div>
                    {content}
                </div>
            </div>
        )
    }
}

export default ModalForm;
