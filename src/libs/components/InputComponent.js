import React, { Component } from "react";
import PropTypes from "prop-types";

export class Input extends Component {
    constructor(props) {
        super(props);
    }

    static get propTypes() {
        return {
            typeInput: PropTypes.string,
            modified: PropTypes.string,
            modifiedContainer: PropTypes.string,
            modifiedLabel: PropTypes.string,
            placeholder: PropTypes.string,
            labelText: PropTypes.string,
            setValue: PropTypes.func,
            value: PropTypes.oneOfType([
                PropTypes.string,
                PropTypes.number,
            ]),
            icon: PropTypes.string,
            readOnlyStatus: PropTypes.bool,
            onValidate: PropTypes.func,
            onBlur: PropTypes.func,
            onFocus: PropTypes.func,
        }
    }

    static get defaultProps() {
        return {
            typeInput: 'text',
            modified: '',
            modifiedContainer: '',
            modifiedLabel: '',
            placeholder: '',
            labelText: '',
            setValue: () => {},
            value: '',
            icon: '',
            readOnlyStatus: false,
            onValidate: () => {},
            onBlur: () => {},
            onFocus: () => {},
        }
    }

    render() {
        const {
            labelText,
            modified,
            placeholder,
            readOnlyStatus,
            modifiedContainer,
            modifiedLabel,
            icon,
            onValidate,
            typeInput,
            setValue,
            value,
            onBlur,
            onFocus,
        } = this.props;

        return (
            <div className={`inputDiv ${modifiedContainer}`}>
                {labelText && (
                    <label
                        className={modifiedLabel}>
                        {labelText}
                    </label>
                )}
                <div className="input-block">
                    <input
                        type={typeInput}
                        className={modified}
                        value={value}
                        placeholder={placeholder}
                        onChange={e => setValue(e.target.value)}
                        readOnly={readOnlyStatus}
                        onBlur={() => {
                            onValidate();
                            onBlur();
                        }}
                        onFocus={onFocus}
                    />
                    {icon && (
                        <div
                            className="icon"
                        >
                            <i className={icon}></i>
                        </div>
                    )}
                </div>
            </div>
        )
    }
}

export default Input;
