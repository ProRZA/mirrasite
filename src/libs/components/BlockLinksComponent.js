import React, { Component } from "react";
import PropTypes from 'prop-types';
import { Link } from "react-router-dom";

class BlockLinks extends Component {
    constructor(props) {
        super(props);
    }

    static get propTypes() {
        return {
            title: PropTypes.string,
            links: PropTypes.arrayOf(PropTypes.shape()),
        }
    }

    static get defaultProps() {
        return {
            title: '',
            links: [],
        }
    }

    render() {
        const { title, links } = this.props;
        return (
            <div className="service__content_block heightContent">
                <div className="background"></div>
                <div className="service__content_caption">{title}</div>
                <div className="service__content-text">
                    {links.map((item, index) => {
                        return (
                            <Link
                                key={index}
                                to={item.target}
                                className="service_link an-scale"
                            >
                                {item.title}
                                <i className="ml-10 far fa-caret-square-right"></i>
                            </Link>
                        )
                    })}
                </div>
            </div>
        )
    }
}

export default BlockLinks;
