import React, { Component } from "react";
import PropTypes from "prop-types";

export class TextArea extends Component {
    constructor(props) {
        super(props);
        this.state = {
            value: '',
        }
    }

    static get propTypes() {
        return {
            modified: PropTypes.string,
            modifiedContainer: PropTypes.string,
            placeholder: PropTypes.string,
            labelText: PropTypes.string,
            maxChars: PropTypes.number,
            setValue: PropTypes.func,
        }
    }

    static get defaultProps() {
        return {
            modified: '',
            modifiedContainer: '',
            placeholder: '',
            labelText: '',
            maxChars: -1,
            setValue: () => {},
        }
    }

    changeValue = (v) => {
        const { setValue } = this.props;
        setValue(v);
        this.setState({value: v});
    };

    render() {
        const {
            labelText,
            mask,
            modified,
            placeholder,
            postSuffix,
            maxChars,
            setValue,
            modifiedContainer,
        } = this.props;
        const { value } = this.state;
        return (
            <div className={`${modifiedContainer}`}>
                <label className="">{labelText}</label>
                {maxChars !== -1 &&
                    <div className="greyText">{`Количество знаков: ${value.length} из ${maxChars}`}</div>
                }
                <textarea
                    className={`TextArea ${modified}`}
                    placeholder={placeholder}
                    value={value}
                    onChange={event => this.changeValue(event.target.value)}
                />
            </div>
        );
    }
}

export default TextArea;
