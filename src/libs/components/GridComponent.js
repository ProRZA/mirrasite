import React, { Component } from "react";
import PropTypes from 'prop-types';

class BlockGrid extends React.PureComponent {
    constructor(props) {
        super(props);
    }

    static get propTypes() {
        return {
            width: PropTypes.number,
            modified: PropTypes.string,
            content: PropTypes.shape(),
        }
    }

    static get defaultProps() {
        return {
            width: 0,
            modified: '',
            content: {},
        }
    }

    render() {
        const { modified, width, content, marginLeft } = this.props;

        return (
            <div
                className={modified}
                style={{width: `${width}%`}}
            >
                {content}
            </div>
        );
    }
}

class Grid extends Component {
    constructor(props) {
        super(props);
    }

    static get propTypes() {
        return {
            modified: PropTypes.string,
            columns: PropTypes.arrayOf(PropTypes.shape()),
        }
    }

    static get defaultProps() {
        return {
            modified: '',
            columns: [],
        }
    }

    render() {
        const { columns, modified } = this.props;

        return (
            <div
                className={`grid-container ${modified}`}
            >
                {columns.map((item, index) => {
                    const w = item.column < 0 ? 'auto' : item.column * 100 / 24;
                    return (
                        <BlockGrid
                            key={index}
                            width={w}
                            modified={item.modified}
                            content={item.content}
                        />
                    )
                })}
            </div>
        )
    }
}

export default Grid;
