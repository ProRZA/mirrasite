import React, { Component } from "react";
import PropTypes from "prop-types";

export class Chekbox extends Component {
    constructor(props) {
        super(props);
        this.state = {
            checked: props.checkDefault,
        };
    }

    static get propTypes() {
        return {
            modified: PropTypes.string,
            labelText: PropTypes.string,
            modifiedContainer: PropTypes.string,
            checkDefault: PropTypes.bool,
            click: PropTypes.func,
            params: PropTypes.oneOfType([
                PropTypes.shape(),
                PropTypes.number,
            ]),
        }
    }

    static get defaultProps() {
        return {
            modified: '',
            labelText: '',
            modifiedContainer: '',
            checkDefault: false,
            click: () => {},
            params: {},
        }
    }

    render() {
        const {
            modified,
            labelText,
            modifiedContainer,
            click,
            params,
        } = this.props;
        const { checked } = this.state;

        let check = (checked ? "checked" : "");
        return (
            <div
                className={`checkbox-area ${modifiedContainer}`}
                onClick={() => {
                    click(params, !checked);
                    this.setState({checked: !checked});
                }}
            >
                <i className={`checkbox ${check}`}></i>
                <span>{labelText}</span>
            </div>
        )
    }
}

export default Chekbox;
