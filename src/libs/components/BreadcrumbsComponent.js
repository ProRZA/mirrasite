import React, { Component } from "react";
import PropTypes from 'prop-types';

import { Link } from "react-router-dom";

export class Breadcrumbs extends React.PureComponent {
    constructor(props) {
        super(props);
    }

    static get propTypes() {
        return {
            links: PropTypes.arrayOf(
                PropTypes.shape()
            ).isRequired,
            currentPage: PropTypes.string,
        }
    }

    static get defaultProps() {
        return {
            currentPage: '',
        }
    }

    render() {
        const { links, currentPage } = this.props;

        return (
            <div className="breadcrumbs">
                <div className="container_ opposite-sides">
                    <div className="current-page">{currentPage}</div>
                    <ul className="breadcrumb">
                        {links.map((item, index) => (
                            <li key={index}>
                                <Link to={item.target}>{item.title}</Link>
                            </li>
                        ))}
                    </ul>
                </div>
            </div>
        )
    }
}

export default Breadcrumbs;
