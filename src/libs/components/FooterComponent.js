import React, { Component } from "react";
import PropTypes from 'prop-types';

export class Footer extends Component {
    constructor(props) {
        super(props);
    }

    static get propTypes() {
        return {

        }
    }

    static get defaultProps() {
        return {

        }
    }

    render() {
        return (
            <footer className="footer">
                <div className="container_ footer__flex">
                    <div className="footer__item">
                        <div className="footer__caption">Адрес</div>
                        <div className="footer__block">
                            <ul className="footer__contact">
                                <li><i className="fas fa-home"></i><span className="footer__text_address ml-5">Медицинская клиника МК-Мед</span></li>
                                <li><span className="footer__text_address">Ланское шоссе 14А</span></li>
                            </ul>
                        </div>
                    </div>

                    <div className="footer__item">
                        <div className="footer__caption">График работы</div>
                        <div className="footer__block_address">
                            <ul className="footer__contact">
                                <li><span
                                    className="footer__text_address">ПОНЕДЕЛЬНИК - СУББОТА: с 10:00 до 21:00</span></li>
                                <li><span className="footer__text_address">ВОСКРЕСЕНЬЕ: выходной</span></li>
                            </ul>
                        </div>
                    </div>

                    <div className="footer__item">
                        <div className="footer__caption">Контакты</div>
                        <div className="footer__block">
                            <ul className="footer__contact">
                                <li><i className="far fa-envelope"></i><span className="footer__text">Email: support@email.com</span>
                                </li>
                                <li><i className="fas fa-phone-alt"></i><span className="footer__text">Телефон: +7(952) 279-78-50</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div className="social">
                    <div className="container_ social__flex">
                        <div className="allright"><i className="far fa-copyright"></i> 2023 «Медицинская клиника». Все
                            права защищены.
                        </div>
                        <div className="social__block">
                            <ul className="soc">
                                <li><a href="#" className="icon"><i className="fab fa-facebook-f"></i></a></li>
                                <li>
                                    <a
                                        href="https://www.instagram.com/mariya_yurkovskaya/"
                                        className="icon"
                                        target="_blank"
                                    >
                                        <i className="fab fa-instagram"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </footer>
        )
    }
}

export default Footer;
