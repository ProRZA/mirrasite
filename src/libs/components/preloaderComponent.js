import React, { Component } from "react";
import PropTypes from "prop-types";

export class Preloader extends Component {
    constructor(props) {
        super(props);
    }

    static get propTypes() {
        return {
            text: PropTypes.string,
        }
    }

    static get defaultProps() {
        return {
            text: '',
        }
    }

    render() {
        const { text } = this.props;
        return (
            <div className="load-wrap">
                <div className="load">
                    <p className="preloader-title">text</p>
                    <div className="line"></div>
                    <div className="line"></div>
                    <div className="line"></div>
                </div>
            </div>
        )
    }
}
