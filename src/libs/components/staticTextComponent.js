import React, { Component } from "react";
import PropTypes from "prop-types";

export class StaticText extends Component {
    constructor(props) {
        super(props);
        this.state = {
            value: '',
        }
    }

    static get propTypes() {
        return {
            modified: PropTypes.string,
            text: PropTypes.string,
        }
    }

    static get defaultProps() {
        return {
            modified: '',
            text: '',
        }
    }

    render() {
        const {
            modified,
            text,
        } = this.props;
        const {
            value,
        } = this.state;

        return (
            <div className={modified}>
                {text}
            </div>
        )
    }
}

export default StaticText;
