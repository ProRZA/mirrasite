import React, { Component } from "react";
import PropTypes from 'prop-types';

import { Link } from "react-router-dom";
import Authorizate from '../../pages-site/components/AuthorizateComponent';
import ModalForm from './ModalFormComponent';
import Grid from './GridComponent';
import EditText from "./EditTextComponent";
import Cart from '../../pages-site/components/CartComponent';
import MainMenu from "../../pages-site/components/MainMenuComponent";

const menuData = [
    {
        title: 'Услуги',
        drop: true,
        links: [
            {
                title: 'Анализы',
                drop: false,
                links: ""
            },
            {
                title: 'Диагностика',
                drop: false,
                links: ""
            },
        ]
    },
    {
        title: 'Косметология',
        drop: true,
        links: [
            {
                title: 'Чистка',
                drop: false,
                links: ""
            },
            {
                title: 'Пилинг',
                drop: false,
                links: ""
            },
            {
                title: 'Ботулинотерапия',
                drop: false,
                links: ""
            },
            {
                title: 'Биоревитализация',
                drop: false,
                links: ""
            },
            {
                title: 'Мезотерапия',
                drop: false,
                links: ""
            },
            {
                title: 'Плазмотерапия',
                drop: false,
                links: ""
            },
            {
                title: 'Биорепарация',
                drop: false,
                links: ""
            },
            {
                title: 'Контурная пластика',
                drop: false,
                links: ""
            },
            {
                title: 'Нити',
                drop: false,
                links: ""
            },
            {
                title: 'Удаление новообразований',
                drop: false,
                links: ""
            },
            {
                title: 'Эпиляция',
                drop: false,
                links: ""
            }
        ]
    },
    {
        title: 'Купить косметику',
        drop: false,
        links: "/shop"
    },
    {
        title: 'Статьи',
        drop: false,
        links: ""
    }
];

export class Header extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showLoginForm: false,
            openMiniMenu: false,
        }
    }

    static get propTypes() {
        return {

        }
    }

    static get defaultProps() {
        return {

        }
    }

    render() {
        const { showLoginForm } = this.state;
        const { openMiniMenu } = this.state;

        return (
            <>
                {showLoginForm && (
                    <ModalForm
                        content={
                            <Authorizate/>
                        }
                        closeForm={() => this.setState({ showLoginForm: false })}
                    />
                )}
                <header className="header">
                    <div className="container_ header__flex">
                        <Grid
                            modified="block_logo_search"
                            columns={[
                                {
                                    column: 6,
                                    modified: 'ml-10',
                                    content: (
                                        <div className="header__logo">
                                            <Link className="header__logo_img" to="/"></Link>
                                        </div>
                                    )
                                },
                                {
                                    column: 18,
                                    modified: 'ml-10',
                                    content: (
                                        <>
                                            <div className="search__block">
                                                <EditText
                                                    modifiedInput="field-search"
                                                    placeholder="Поиск"
                                                    buttons={[
                                                        {
                                                            modified: "bg-transparent br-none csr-pointer",
                                                            icon: "fa fa-search",
                                                            click: () => {
                                                            },
                                                        }
                                                    ]}
                                                />
                                            </div>
                                            <div className="navigation_minimenu">
                                                <div
                                                    className="mini"
                                                    className={`mini ${openMiniMenu ? 'change' : ''}`}
                                                    onClick={() => this.setState({ openMiniMenu: !openMiniMenu })}
                                                >
                                                    <div className="bar1"></div>
                                                    <div className="bar2"></div>
                                                    <div className="bar3"></div>
                                                </div>
                                            </div>
                                        </>
                                    )
                                },
                            ]}
                        />
                        <div className="column block_cart_auth">
                            <Cart/>
                            <div className="autorizated">
                                <a
                                    href="#"
                                    className="link login an-scale"
                                    onClick={() => this.setState({ showLoginForm: true })}
                                >
                                    <span>Авторизоваться</span>
                                    <i className="fa fa-lock"></i>
                                </a>
                            </div>
                        </div>
                        <div className="nav">
                            <MainMenu
                                options={menuData}
                                openMiniMenu={openMiniMenu}
                            />
                        </div>
                    </div>
                </header>
            </>
        )
    }
}

export default Header;
