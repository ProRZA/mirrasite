import React, { Component } from "react";
import PropTypes from "prop-types";

export class Button extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modifiedButton: '',
        }
    }

    static get propTypes() {
        return {
            fontModified: PropTypes.string,
            modified: PropTypes.string,
            caption: PropTypes.string,
            icon: PropTypes.string,
            click: PropTypes.func,
            styles: PropTypes.shape(),
        }
    }

    static get defaultProps() {
        return {
            fontModified: '',
            modified: '',
            caption: '',
            icon: '',
            styles: {},
            click: () => {},
        }
    }

    render() {
        const {
            fontModified,
            caption,
            modified,
            click,
            icon,
            styles,
        } = this.props;
        const {
            modifiedButton,
        } = this.state;

        // const iconView = () => {
        //     switch (icon) {
        //         case 'add': return (<i className={`fas fa-plus-circle`}></i>);
        //         case 'edit': return (<i className={`fas fa-pencil-alt`}></i>);
        //         case 'delete': return (<i className={`fas fa-trash-alt`}></i>);
        //         default: return (<i></i>);
        //     }
        // };

        return (
            <button
                className={`${modifiedButton} ${modified}`}
                style={styles}
                onClick={click}
                >
                    <div className={fontModified}>
                        {caption}
                    </div>
                    {icon.length > 0 && (
                        <div className="">
                            <i className={icon}></i>
                        </div>
                    )}
            </button>
        )
    }
}

export default Button;
