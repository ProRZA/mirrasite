export const receiveExistGoods = data => ({ type: 'RECEIVE_EXIST_GOODS', data });
export const requestExistGoods = () => ({ type: 'REQUEST_EXIST_GOODS' });
export const errorExistGoods = (error) => ({ type: 'ERROR_EXIST_GOODS', error });

import config from '../../config.app';

export function fetchExistGoods(params) {
    return (dispatch, getState) => {
        dispatch(requestExistGoods());
        const state = getState();
        fetch(config.urls.checkExistsGoods, {
            method: 'POST',
            body: JSON.stringify(params),
            headers: {
                Accept: 'application/json',
                'content-Type': 'application/json',
            }
        })
            .then(response => response.json())
            .then(data => dispatch(receiveExistGoods(data)))
            .catch(err => dispatch(errorExistGoods(err)));
    };
}
