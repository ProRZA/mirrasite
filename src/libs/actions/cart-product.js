export const receiveCartProduct = data => ({ type: 'RECEIVE_CART_PRODUCT', data });
export const requestCartProduct = () => ({ type: 'REQUEST_CART_PRODUCT' });
export const errorCartProduct = (error) => ({ type: 'ERROR_CART_PRODUCT', error });

import config from '../../config.app';

export function fetchCartProduct(params) {
    return (dispatch, getState) => {
        dispatch(requestCartProduct());
        const state = getState();
        fetch(config.urls.cartProduct, {
            method: 'POST',
            body: JSON.stringify(params),
            headers: {
                Accept: 'application/json',
                'content-Type': 'application/json',
            }
        })
            .then(response => response.json())
            .then(data => dispatch(receiveCartProduct(data)))
            .catch(err => dispatch(errorCartProduct(err)));
    };
}
