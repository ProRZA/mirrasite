export const receiveFilters = data => ({ type: 'RECEIVE_FILTERS', data });
export const requestFilters = () => ({ type: 'REQUEST_FILTERS' });
export const errorFilters = (error) => ({ type: 'ERROR_FILTERS', error });

import config from '../../config.app';

export function fetchFilters() {
    return (dispatch, getState) => {
        dispatch(requestFilters());
        const state = getState();
        const url = config.urls.filters;
        fetch(url, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'content-Type': 'application/json',
            }
        })
            .then(response => response.json())
            .then(data => dispatch(receiveFilters(data)))
            .catch(err => dispatch(errorFilters(err)));
    };
}
