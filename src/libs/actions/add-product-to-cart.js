export const addCart = data => ({ type: 'ADD_CART', data });
export const removeCart = id => ({ type: 'REMOVE_CART', id });
export const errorCart = (error) => ({ type: 'ERROR_CART', error });
export const resetCart = () => ({ type: 'RESET_CART' });
export const changeCart = (data) => ({ type: 'CHANGE_CART', data });
