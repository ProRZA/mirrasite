export const receiveAddGoods = data => ({ type: 'RECEIVE_ADD_GOODS', data });
export const requestAddGoods = () => ({ type: 'REQUEST_ADD_GOODS' });
export const errorAddGoods = (error) => ({ type: 'ERROR_ADD_GOODS', error });
export const resetAddGoods = () => ({ type: 'RESET_ADD_GOODS' });

export const errorLoadImage = (error) => ({ type: 'ERROR_LOAD_IMAGE', error });

const axios = require('axios');
import config from '../../config.app';

export function fetchAddGoods(params) {
    return (dispatch, getState) => {
        dispatch(requestAddGoods());
        const formData = new FormData();
        formData.append('photo', params.image);
        const header = {
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        };
        axios.post(config.urls.loadImage,formData,header)
            .then(response => {
                const goods = {
                    ...params, image: response.data.pathToFile
                };
                return fetch(config.urls.goodsAdd, {
                    method: 'POST',
                    body: JSON.stringify(goods),
                    headers: {
                        Accept: 'application/json',
                        'content-Type': 'application/json',
                    }
                })
                    .then(response => response.json())
                    .then(data => dispatch(receiveAddGoods(data)))
                    .catch(err => dispatch(errorAddGoods(err)))
            })
            .catch(err => dispatch(errorLoadImage(err)));
    };
}
