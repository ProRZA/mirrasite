export const receiveGoods = data => ({ type: 'RECEIVE_GOODS', data });
export const requestGoods = () => ({ type: 'REQUEST_GOODS' });
export const errorGoods = (error) => ({ type: 'ERROR_GOODS', error });
export const resetGoods = () => ({ type: 'RESET_GOODS' });

import config from '../../config.app';

export function fetchGoods(params = []) {
    return dispatch => {
        dispatch(requestGoods());
        const url = config.urls.goods;
        return fetch(url, {
            method: 'POST',
            body: JSON.stringify(params),
            headers: {
                Accept: 'application/json',
                'content-Type': 'application/json',
            }
        })
            .then(response => response.json())
            .then(data => dispatch(receiveGoods(data)))
            .catch(err => dispatch(errorGoods(err)))
    };
}
