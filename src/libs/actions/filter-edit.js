export const receiveFilterEdit = (data) => ({ type: 'RECEIVE_FILTER_EDIT', data });
export const requestFilterEdit = () => ({ type: 'REQUEST_FILTER_EDIT' });
export const errorFilterEdit = (error) => ({ type: 'ERROR_FILTER_EDIT', error });
export const resetFilterEdit = () => ({ type: 'RESET_FILTER_EDIT' });

import config from '../../config.app';

export function fetchFilterEdit(params) {
    return (dispatch, getState) => {
        let url = '';
        let method = '';
        switch (params.action) {
            case 'add':
                url = config.urls.filterAdd;
                method = 'POST';
                break;
            case 'delete':
                url = config.urls.filterDelete;
                method = 'DELETE';
                break;
            case 'edit':
                url = config.urls.filterEdit;
                method = 'PATCH';
                break;
            default: return;
        }

        dispatch(requestFilterEdit());
        const state = getState();
        fetch(url, {
            method,
            body: JSON.stringify(params),
            headers: {
                Accept: 'application/json',
                'content-Type': 'application/json',
            }
        })
            .then(response => response.json())
            .then(data => dispatch(receiveFilterEdit(data)))
            .catch(err => dispatch(errorFilterEdit(err)));
    };
}
