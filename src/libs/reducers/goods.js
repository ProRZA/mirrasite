export default function goods(state, action) {
    if (typeof state === "undefined") {
        return {
            isFetching: false,
            data: null,
            error: null,
        };
    }

    switch (action.type) {
        case 'REQUEST_GOODS':
            return Object.assign({}, state, {
                ...state,
                isFetching: true,
            });
        case 'RECEIVE_GOODS':
            return Object.assign({}, state, {
                ...state,
                isFetching: false,
                data: action.data,
            });
        case 'ERROR_GOODS':
            return Object.assign({}, state, {
                ...state,
                isFetching: false,
                error: action.error,
            });
        case 'RESET_GOODS':
            return Object.assign({}, state, {
                ...state,
                isFetching: false,
                data: null,
                error: null,
            });
        default:
            return state;
    }
}
