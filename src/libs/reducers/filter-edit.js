export default function filtersEdit(state, action) {
    if (typeof state === "undefined") {
        return {
            isFetching: false,
            data: null,
            error: null,
        };
    }

    switch (action.type) {
        case 'REQUEST_FILTER_EDIT':
            return Object.assign({}, state, {
                ...state,
                isFetching: true,
            });
        case 'RECEIVE_FILTER_EDIT':
            return Object.assign({}, state, {
                ...state,
                isFetching: false,
                data: action.data,
            });
        case 'ERROR_FILTER_EDIT':
            return Object.assign({}, state, {
                ...state,
                isFetching: false,
                error: action.error,
            });
        case 'RESET_FILTER_EDIT':
            return Object.assign({}, state, {
                ...state,
                isFetching: false,
                data: null,
                error: null,
            });
        default:
            return state;
    }
}
