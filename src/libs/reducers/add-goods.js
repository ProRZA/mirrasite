export default function addGoods(state, action) {
    if (typeof state === "undefined") {
        return {
            isFetching: false,
            data: null,
            error: null,
        };
    }

    switch (action.type) {
        case 'REQUEST_ADD_GOODS':
            return Object.assign({}, state, {
                ...state,
                isFetching: true,
            });
        case 'RECEIVE_ADD_GOODS':
            return Object.assign({}, state, {
                ...state,
                isFetching: false,
                data: action.data,
            });
        case 'ERROR_ADD_GOODS':
            return Object.assign({}, state, {
                ...state,
                isFetching: false,
                error: action.error,
            });
        case 'RESET_ADD_GOODS':
            return Object.assign({}, state, {
                ...state,
                isFetching: false,
                data: null,
                error: null,
            });
        case 'ERROR_LOAD_IMAGE':
            return Object.assign({}, state, {
                ...state,
                isFetching: false,
                error: action.error,
            });
        default:
            return state;
    }
}
