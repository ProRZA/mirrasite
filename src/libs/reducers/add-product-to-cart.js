export default function cart(state, action) {
    if (typeof state === "undefined") {
        return {
            ids: [],
            title: null,
            totalPrice: 0,
            error: null,
        };
    }

    switch (action.type) {
        case 'ADD_CART':
            return Object.assign({}, state, addProduct(state, action.data));
        case 'REMOVE_CART':
            return Object.assign({}, state, removeProduct(state, action.id));
        case 'ERROR_CART':
            return Object.assign({}, state, {
                ...state,
                error: action.error,
            });
        case 'CHANGE_CART':
            return Object.assign({}, state, changeCart(state, action.data));
        case 'RESET_CART':
            return Object.assign({}, state, {
                ...state,
                ids: [],
                title: null,
                totalPrice: 0,
                error: null,
            });
        default:
            return state;
    }
}

function addProduct(state, data) {
    let ids = [...state.ids];
    let product = ids.filter(item => item.id === data.id);
    if (product.length === 1) {
        product = product[0];
        product.count++;
    } else {
        product = data;
        product.count = 1;
        ids = [...ids, product];
    }
    const count = ids.reduce((acc, cur) => acc + cur.count, 0);
    const title = `${count} шт.`;
    const totalPrice = ids.reduce((acc, cur) => acc + cur.price*cur.count, 0);
    return {
        ids, title, totalPrice,
    }
}

function removeProduct(state, id) {
    let ids = [...state.ids];
    let title;
    let product = ids.filter(item => item.id === id);
    if (product.length === 1) {
        product = product[0];
        if (product.count === 1) {
            ids = ids.filter(item => item.id !== id);
            title = null;
        } else {
            product.count--;
            const count = ids.reduce((acc, cur) => acc + cur.count, 0);
            title = `${count} шт.`;
        }
    }
    const totalPrice = ids.reduce((acc, cur) => acc + cur.price*cur.count, 0);
    return {
        ids, title, totalPrice,
    }
}

function changeCart(state, data) {
    const ids = state.ids.map(item => item.id === data.id ? data : item);
    const totalPrice = ids.reduce((acc, cur) => acc + cur.price*cur.count, 0);
    return {
        ids, title: state.title, totalPrice,
    }
}
