export default function existGoods(state, action) {
    if (typeof state === "undefined") {
        return {
            isFetching: false,
            data: null,
            error: null,
        };
    }

    switch (action.type) {
        case 'REQUEST_EXIST_GOODS':
            return Object.assign({}, state, {
                ...state,
                isFetching: true,
            });
        case 'RECEIVE_EXIST_GOODS':
            return Object.assign({}, state, {
                ...state,
                isFetching: false,
                data: action.data,
            });
        case 'ERROR_EXIST_GOODS':
            return Object.assign({}, state, {
                ...state,
                isFetching: false,
                error: action.error,
            });
        default:
            return state;
    }
}
