export default function cartProduct(state, action) {
    if (typeof state === "undefined") {
        return {
            isFetching: false,
            data: null,
            error: null,
        };
    }

    switch (action.type) {
        case 'REQUEST_CART_PRODUCT':
            return Object.assign({}, state, {
                ...state,
                isFetching: true,
            });
        case 'RECEIVE_CART_PRODUCT':
            return Object.assign({}, state, {
                ...state,
                isFetching: false,
                data: action.data,
            });
        case 'ERROR_CART_PRODUCT':
            return Object.assign({}, state, {
                ...state,
                isFetching: false,
                error: action.error,
            });
        case 'RESET_CART_PRODUCT':
            return Object.assign({}, state, {
                ...state,
                isFetching: false,
                data: null,
                error: null,
            });
        default:
            return state;
    }
}
