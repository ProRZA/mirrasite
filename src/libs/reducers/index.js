import { combineReducers } from "redux";
import filters from "./filters";
import filtersEdit from "./filter-edit";
import goods from "./goods";
import cartProduct from "./cart-product";
import cart from "./add-product-to-cart";
import addGoods from './add-goods';
import existGoods from './exist-goods';

export const rootReducer = combineReducers({
    filters,
    filtersEdit,
    goods,
    cartProduct,
    cart,
    addGoods,
    existGoods,
});
