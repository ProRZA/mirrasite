export default function filters(state, action) {
    if (typeof state === "undefined") {
        return {
            isFetching: false,
            data: null,
            error: null,
        };
    }

    switch (action.type) {
        case 'REQUEST_FILTERS':
            return Object.assign({}, state, {
                ...state,
                isFetching: true,
            });
        case 'RECEIVE_FILTERS':
            return Object.assign({}, state, {
                ...state,
                isFetching: false,
                data: action.data,
            });
        case 'ERROR_FILTERS':
            return Object.assign({}, state, {
                ...state,
                isFetching: false,
                error: action.error,
            });
        case 'RESET_FILTERS':
            return Object.assign({}, state, {
                ...state,
                isFetching: false,
                data: null,
                error: null,
            });
        default:
            return state;
    }
}
