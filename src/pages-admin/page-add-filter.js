import React, { Component } from "react";
import PropTypes from "prop-types";
import {connect} from "react-redux";

import EditText from "../libs/components/EditTextComponent";

import { fetchFilters } from '../libs/actions/filters';
import { fetchFilterEdit } from "../libs/actions/filter-edit";

class PageAddFilter extends Component {
    constructor(props) {
        super(props);
        props.dispatch(fetchFilters());
    }

    static get propTypes() {
        return {
            filters: PropTypes.shape(),
            filtersEdit: PropTypes.shape(),
            dispatch: PropTypes.func,
        }
    };

    static get defaultProps() {
        return {
            filters: {},
            filtersEdit: {},
            dispatch: () => {},
        }
    };

    componentDidUpdate(prevProps, prevState) {
        const { dispatch } = this.props;
        if (prevProps.filtersEdit.isFetching !== this.props.filtersEdit.isFetching) {
            dispatch(fetchFilters());
        }
    };

    filterAdd = (value, filter_name) => {
        const { dispatch } = this.props;
        const params = {
            action: 'add',
            filter_name,
            value,
        };
        dispatch(fetchFilterEdit(params));
    };

    filterEdit = (value, id) => {
        const { dispatch } = this.props;
        const params = {
            action: 'edit',
            id,
            value,
        };
        dispatch(fetchFilterEdit(params));
    };

    filterDelete = (value, id) => {
        const { dispatch } = this.props;
        const params = {
            action: 'delete',
            id,
        };
        dispatch(fetchFilterEdit(params));
    };

    getFilters = () => {
        return [
            { filter_name: 'illness', name: 'Тип болезни' },
            { filter_name: 'ages', name: 'Возраст' },
            { filter_name: 'skin', name: 'Тип кожи' },
        ]
    };

    render() {
        const { filters } = this.props;
        const filtersList = this.getFilters();

        return(
           <div className="container_">
               <p className="caption-main text-align-center">Страница редактирования фильтров</p>
               <div className="filters-content">
                   {filtersList.map((item, index) => {
                       return (
                           <div key={index} className="row">
                               <p>{item.name}</p>
                               <div>
                                   {filters.data?.map(filter => {
                                       if (filter.filter_name === item.filter_name) {
                                           return (
                                               <EditText
                                                   key={filter.id}
                                                   buttons={[
                                                       {
                                                           icon: 'edit',
                                                           click: this.filterEdit,
                                                       },
                                                       {
                                                           icon: 'delete',
                                                           click: this.filterDelete,
                                                       },
                                                       ]
                                                    }
                                                   value={filter.value}
                                                   params={filter.id}
                                               />
                                           )
                                       }
                                   })}
                               </div>
                           </div>
                       )
                   })}
               </div>
               <div className="filters-content">
                   {filtersList.map((item, index) => {
                       return (
                           <div key={index} className="row">
                               <EditText
                                   key={index}
                                   placeholder="Новое значения для фильтра"
                                   buttons={[
                                       {
                                           icon: 'add',
                                           click: this.filterAdd,
                                       }]
                                   }
                                   params={item.filter_name}
                                   clearAfterSend
                               />
                           </div>
                       )
                   })}
               </div>
           </div>
        )
    }
}

export default connect((state) => {
    const {
        filters,
        filtersEdit,
        dispatch,
    } = state;
    return {
        filters,
        filtersEdit,
        dispatch,
    };
})(PageAddFilter);

