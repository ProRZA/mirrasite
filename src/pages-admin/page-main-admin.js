import React, { Component } from "react";
import { Link } from "react-router-dom";

import PageAddFilter from "./page-add-filter";
import PageHome from "../pages-site/page-home";
import PageAddGoods from "./page-add-goods";
import PageShop from "../pages-site/page-shop";

class PageMainAdmin extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return(
            <div className="container_">
                <ul>
                    <li><Link to="/admin/goods">Добавление товаров</Link></li>
                    <li><Link to="/admin/filters">Добавление фильтров</Link></li>
                </ul>
            </div>
        )
    }
}

export default PageMainAdmin;

