import React, { Component } from "react";
import PropTypes from "prop-types";
import {connect} from "react-redux";

import { fetchFilters } from "../libs/actions/filters";
import { fetchAddGoods } from '../libs/actions/add-goods';
import { fetchExistGoods } from '../libs/actions/exist-goods';

import { Input } from "../libs/components/InputComponent";
import { InputSelect } from "../libs/components/InputSelectComponent";
import { TextArea } from "../libs/components/TextAreaComponent";
import { Button } from "../libs/components/ButtonComponent";
import { Chekbox } from "../libs/components/CheckboxComponent";

class PageAddGoods extends Component {
    constructor (props) {
        super(props);
        const {dispatch } = this.props;
        this.state = {
            values: {
                brand: '',
                name: '',
                description: '',
                price: '',
                filter: [],
                file: null,
            },
            error: '',
        };
        this.refFile = React.createRef();
        dispatch(fetchFilters());
    }

    static get propTypes() {
        return {
            filters: PropTypes.shape(),
            addGoods: PropTypes.shape(),
            checkExistsGoods: PropTypes.shape(),
            dispatch: PropTypes.func,
        }
    };

    static get defaultProps() {
        return {
            filters: {},
            addGoods: {},
            dispatch: () => {},
        }
    };

    addProduct = () => {
        const { dispatch } = this.props;
        const { values } = this.state;
        const {
            name,
            brand,
            description,
            price,
            filter,
            file
        } = values;

        if (Object.keys(values).some((key, value) => (values[key]?.length === 0 || !values[key]))) {
            this.setState({ error: 'Не все поля заполнены' });
            window.scrollTo({ top: 0, behavior: 'smooth' })
        } else {
            this.setState( { error: '' });
            const params = {
                name,
                brand,
                description,
                price,
                filter,
                image: file,
            };

            dispatch(fetchAddGoods(params));
        }
    };

    changeFilter = (id, checked) => {
        const { values } = this.state;
        const { filter } = values;

        if (checked) {
            this.setState({values: {...values, filter: [...filter, id]}})
        } else {
            this.setState({values: {...values, filter: values.filter.filter(item => item !== id)}})
        }
    };

    getFilters = () => {
        return [
            { filter_name: 'illness', name: 'Тип болезни' },
            { filter_name: 'ages', name: 'Возраст' },
            { filter_name: 'skin', name: 'Тип кожи' },
        ]
    };

    render() {
        const { filters, dispatch, existGoods } = this.props;
        const { values, error } = this.state;
        const {
            brand,
            name,
            price,
            file,
        } = values;
        const filtersList = this.getFilters();

        return (
            <div className="container_">
                <p className="caption-main text-align-center">Страница редактирования товаров</p>
                <p className="cetner-block error">{error}</p>
                <div className="w-70">
                    <InputSelect
                        modifiedContainer="mt-10"
                        labelText="Наименование товара"
                        value={name}
                        options={!existGoods.isFetching && existGoods.data?.length > 0 ? existGoods.data.map(item => item.name) : []}
                        setValue={v => {
                            this.setState({ values: {...values, name: v }});
                            if (v.length > 2) dispatch(fetchExistGoods({nameGoods: v}));
                        }}
                    />
                    <Input
                        modifiedContainer="mt-10"
                        labelText="Брэнд товара"
                        value={brand}
                        setValue={v => this.setState({ values: {...values, brand: v }})}
                    />
                    <Input
                        modifiedContainer="mt-10"
                        labelText="Цена товара"
                        value={price}
                        setValue={v => this.setState({ values: {...values, price: v.replace(/^[^0-9.]+$/i,"") }})}
                    />
                    <TextArea
                        modifiedContainer="mt-10"
                        labelText="Описание товара"
                        maxChars={1000}
                        setValue={v => this.setState({ values: {...values, description: v }})}
                    />
                    <div className="line-block">
                        <Button
                            modified="button-simple"
                            caption="Загрузить файл"
                            click={() => this.refFile.current.click()}
                        />
                        <Input
                            placeholder="Название файла"
                            value={file ? file.name : ''}
                            readOnlyStatus={true}
                        />
                        <input
                            ref={this.refFile}
                            type="file"
                            className="display-none"
                            name="photo"
                            onChange={event => this.setState({ values: {...values, file: event.target.files[0] }})}
                        />
                    </div>
                    <div className="filters-content mt-10">
                        {filtersList.map((item, index) => {
                            return (
                                <div key={index} className="row">
                                    <p>{item.name}</p>
                                    <div>
                                        {filters.data?.map(filter => {
                                            if (filter.filter_name === item.filter_name) {
                                                return (
                                                    <Chekbox
                                                        key={filter.id}
                                                        modifiedContainer="mt-5"
                                                        labelText={filter.value}
                                                        click={this.changeFilter}
                                                        params={filter.id}
                                                    />
                                                )
                                            }
                                        })}
                                    </div>
                                </div>
                            )
                        })}
                    </div>

                    <Button
                        modified="button-simple"
                        caption={"Добавить товар в базу"}
                        click={() => this.addProduct()}
                    />
                </div>
            </div>
        )
    }
}

export default connect((state) => {
    const {
        filters,
        addGoods,
        existGoods,
    } = state;
    return {
        filters,
        addGoods,
        existGoods,
    };
})(PageAddGoods);
