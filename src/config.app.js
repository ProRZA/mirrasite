const config = {
    urls: {
        goods: '/api/goods/get',
        goodsAdd: '/api/goods/edit/add',
        goodsDelete: '/api/goods/edit/delete',
        goodsChange: '/api/goods/edit/change',
        cartProduct: '/api/goods/view/single',
        checkExistsGoods: '/api/goods/checkExistsGoods',
        filters: '/api/filters/get',
        filterAdd: '/api/filter/edit/add',
        filterDelete: '/api/filter/edit/delete',
        filterEdit: '/api/filter/edit/edit',
        loadImage: '/api/image/save',
    }
};

export default config;
