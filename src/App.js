import React, { Component } from "react";
import {Switch, Route, BrowserRouter} from 'react-router-dom';

import PageHome from "./pages-site/page-home";
import PageShop from  "./pages-site/page-shop";
import PageMainAdmin from "./pages-admin/page-main-admin";
import PageAddFilter from "./pages-admin/page-add-filter";
import PageAddGoods from "./pages-admin/page-add-goods";
import PageLK from './pages-site/page-lk';
import PageOrder from './pages-site/page-order';
import {Provider} from "react-redux";
import {store} from "./configStore";


class App extends Component {
    render(){
        return(
            <Provider store={store}>
                <BrowserRouter>
                    <Switch>
                        <Route exact path="/" component={PageHome}/>
                        <Route path="/admin/filters" component={PageAddFilter}/>
                        <Route path="/admin/goods" component={PageAddGoods}/>
                        <Route path="/admin" component={PageMainAdmin}/>
                        <Route path="/shop/order" component={PageOrder}/>
                        <Route path="/shop" component={PageShop}/>
                        <Route path="/lk" component={PageLK}/>
                        <Route path="*" render={ () => (<h1>PAGE NOT FOUND</h1>) }/>
                    </Switch>
                </BrowserRouter>
            </Provider>
        )
    }
}

export default App;
