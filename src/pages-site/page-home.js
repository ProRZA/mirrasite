import React, { Component } from "react";

import HeaderMainFooter from '../libs/layout/header-main-footer';
import Article from "../libs/components/ArticleComponent";
import BlockLinks from "../libs/components/BlockLinksComponent";
import Carousel from "./components/CarouselComponent";
import {Link} from "react-router-dom";
import PropTypes from "prop-types";


const text1 = "Прежде чем назначать витамины или капельницы красоты необходимо обследование, а благодаря гормональному скринингу можно прогнозировать скорость омоложения и грамотно разработать программу сохранения здоровья и создать систему омоложения - идеально, как ключ к замку. Анализы можно сдать в любой лаборатории."
const text2 = "Чем отличаются руки без маникюра и после? Те же пальцы, ногти, кутикула. Но это выглядит ухожено.\n" +
    " \n" +
    "Сейчас инъекционные методы поддержания молодости и сохранения красоты так же естественны, как и маникюр. Благодаря новейшим разработкам в анестезии,  процедуры стали безболезненные, легкие в реабилитации, безопасные благодаря высоким стандартам, и с быстрым \"вау\" эффектом. \n" +
    "\n" +
    "Некоторых женщин останавливает миф о том, что еще рано делать инъекции, что же тогда останется на потом?";

class PageHome extends Component {
    constructor(props) {
        super(props);
        this.state = {
            text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore ' +
                'et dolore magna aliqua. Dictum sit amet justo donec enim diam vulputate ut pharetra. Sit amet consectetur ' +
                'adipiscing elit ut aliquam. Feugiat nibh sed pulvinar proin gravida hendrerit lectus a. Sapien faucibus ' +
                'et molestie ac. Quis vel eros donec ac odio tempor. Vel quam elementum pulvinar etiam non. Imperdiet ' +
                'dui accumsan sit amet nulla. Sit amet nisl suscipit adipiscing bibendum est ultricies integer quis. ' +
                'Amet massa vitae tortor condimentum lacinia quis vel eros donec. Massa placerat duis ultricies lacus ' +
                'sed turpis tincidunt. Suspendisse in est ante in nibh mauris. Lacinia at quis risus sed vulputate. Tempor ' +
                'id eu nisl nunc mi ipsum faucibus vitae aliquet.'
        };
    }

    static get propTypes() {
        return {

        }
    }

    static get defaultProps() {
        return {

        }
    }

    render() {
        const { text } = this.state;

        return (
            <HeaderMainFooter
                content={(
                    <div className="">
                        <Carousel
                            modified="carousel__slides"
                            play={true}
                            slides={[
                                {
                                    img: `${process.env.URL}/banners/slider1.jpg`,
                                    content: (
                                        <div className="block-slider">
                                            <div className="block-slider-content">
                                                <div className="block-slider-content_text heightContent">
                                                    <div className="background"></div>
                                                    <div className="text">
                                                        Моя миссия - помогать становиться целостными и  приумножать здоровье сквозь время.
                                                        Цель - раскрыть красоту и сохранить молодость
                                                        1 миллиону человек!
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    ),
                                },
                                {
                                    img: `${process.env.URL}/banners/slider2.jpg`,
                                    content: (
                                        <div className="block-slider">
                                            <div className="block-slider-content">
                                                <div className="block-slider-content_text heightContent">
                                                    <div className="background"></div>
                                                    <div className="text">
                                                        Проект направлен на раскрытие красоты, здоровья и омоложения
                                                        организма, здесь квалифицированные врачи терапевты, гинекологи и
                                                        дерматологи проведут исследования и назначат соответствующую терапию для здоровья и долголетия
                                                        <div className="tag"></div>
                                                        <Link
                                                            to="/"
                                                            className="button-carousel"
                                                        >
                                                            Записаться на прием
                                                        </Link>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    ),
                                },
                                {
                                    img: `${process.env.URL}/banners/slider3.jpg`,
                                    content: (
                                        <div className="block-slider">
                                            <div className="block-slider-content">
                                                <div className="block-slider-caption">Генетическое тестирование</div>
                                                <div className="block-slider-content_text heightContent">
                                                    <div className="background"></div>
                                                    <div className="text">
                                                        Возможность выявить склонность к различным заболеваниям, предсказать
                                                        характер их течения, определить факторы риска, сильные и слабые стороны конкретного пациента
                                                        <div className="tag"></div>
                                                        <Link
                                                            to="/"
                                                            className="button-carousel"
                                                        >
                                                            Заказать ГЕНОТЕСТ
                                                        </Link>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    ),
                                }
                            ]}
                        />
                        <div className="tag"></div>
                        <div className="container_">
                            <section className="service procedure">
                                <Article
                                    title="Услуги"
                                    text={text1}
                                    blockLinks={
                                        <BlockLinks
                                            title='Переход на страницы'
                                            links={[
                                                {
                                                    title: 'Анализы',
                                                    target: '',
                                                },
                                                {
                                                    title: 'Диагностика',
                                                    target: '',
                                                }
                                            ]}
                                        />
                                    }
                                />
                            </section>
                            <section className="service procedure">
                                <Article
                                    title="Косметология"
                                    text={text2}
                                    blockLinks={
                                        <BlockLinks
                                            title='Переход на страницы'
                                            links={[
                                                {
                                                    title: 'Чистка',
                                                    target: '',
                                                },
                                                {
                                                    title: 'Пилинг',
                                                    target: '',
                                                },
                                                {
                                                    title: 'Ботулинотерапия',
                                                    target: '',
                                                },
                                                {
                                                    title: 'Биоревитализация',
                                                    target: '',
                                                },
                                                {
                                                    title: 'Мезотерапия',
                                                    target: '',
                                                },
                                                {
                                                    title: 'Плазмотерапия',
                                                    target: '',
                                                },
                                                {
                                                    title: 'Биорепарация',
                                                    target: '',
                                                },
                                                {
                                                    title: 'Контурная пластика',
                                                    target: '',
                                                },
                                                {
                                                    title: 'Нити',
                                                    target: '',
                                                },
                                                {
                                                    title: 'Удаление новообразований',
                                                    target: '',
                                                },
                                                {
                                                    title: 'Эпиляция',
                                                    target: '',
                                                },
                                            ]}
                                        />
                                    }
                                />
                            </section>
                            <section className="service shop">
                                <Article
                                    title="Купить косметику"
                                    text={text}
                                    blockLinks={
                                        <BlockLinks
                                            title='Переход на страницы'
                                            links={[
                                                {
                                                    title: 'Посетить магазин',
                                                    target: '/shop'
                                                }
                                            ]}
                                        />
                                    }
                                />
                            </section>
                            <section className="service posts">
                                <Article
                                    title="Блог"
                                    text={text}
                                    blockLinks={
                                        <BlockLinks
                                            title='Переход на страницы'
                                            links={[
                                                {
                                                    title: 'Посетить страницу блога',
                                                    target: '',
                                                }
                                            ]}
                                        />
                                    }
                                />
                            </section>
                        </div>
                    </div>
                )}
            />
        )
    }
}

export default PageHome;
