import React, { Component } from "react";
import PropTypes from 'prop-types';

import HeaderSidebarMainFooter from '../libs/layout/header-sidebar-main-footer';
import Grid from '../libs/components/GridComponent';
import Input from '../libs/components/InputComponent';
import Button from '../libs/components/ButtonComponent';

class PageLK extends Component {
    constructor(props) {
        super(props);
    }

    static get propTypes() {
        return {

        }
    }

    static get defaultProps() {
        return {

        }
    }

    render() {
        return (
            <HeaderSidebarMainFooter
                showMainMenu={false}
                content={
                    <div className="container_">
                        <Grid
                            modified="mb-24"
                            columns={[
                                {
                                    column: 14,
                                    content: (
                                        <p className="main-caption ml-20">Личные данные</p>
                                    )
                                },
                                {
                                    column: 10,
                                    content: (
                                        <p className="main-caption">Адрес</p>
                                    )
                                }
                            ]}
                        />
                        <Grid
                            modified="mb-24"
                            columns={[
                                {
                                    column: 6,
                                    modified: "center-block",
                                    content: (
                                        <div>

                                            <div className="lk-foto">
                                                <img src={"#"} alt={''}/>
                                                <span className="text">Нет изображения</span>
                                            </div>
                                        </div>
                                    )
                                },
                                {
                                    column: 8,
                                    content: (
                                        <div className="w-80">
                                            <Input
                                                placeholder="Имя"
                                            />
                                            <Input
                                                modifiedContainer="mt-10"
                                                placeholder="Фамилия"
                                            />
                                            <Input
                                                modifiedContainer="mt-10"
                                                placeholder="Номер телефона"
                                            />
                                            <Input
                                                modifiedContainer="mt-10"
                                                placeholder="E-mail"
                                            />
                                            <Input
                                                modifiedContainer="mt-10"
                                                placeholder="Пароль"
                                            />
                                        </div>
                                    )
                                },
                                {
                                    column: 8,
                                    content: (
                                        <div>
                                            <Input
                                                placeholder="Город"
                                            />
                                            <Input
                                                modifiedContainer="mt-10"
                                                placeholder="Улица"
                                            />
                                            <Grid
                                                modified="mt-10"
                                                columns={[
                                                    {
                                                        column: 11,
                                                        content: (
                                                            <Input
                                                                placeholder="Номер дома"
                                                            />
                                                        )
                                                    },
                                                    {
                                                        column: 2,
                                                        content: (
                                                            <span/>
                                                        )
                                                    },
                                                    {
                                                        column: 11,
                                                        content: (
                                                            <Input
                                                                placeholder="Номер квартиры"
                                                            />
                                                        )
                                                    }
                                                ]}
                                            />
                                        </div>
                                    )
                                },
                            ]}
                        />
                        <div className="right-block">
                            <Button
                                modified="button_lk"
                                caption="Сохранить"
                            />
                        </div>
                    </div>
                }
            />
        )
    }
}

export default PageLK;
