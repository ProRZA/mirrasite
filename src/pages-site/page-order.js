import React, { Component } from "react";
import PropTypes from 'prop-types';

import HeaderSidebarMainFooter from '../libs/layout/header-sidebar-main-footer';
import Grid from '../libs/components/GridComponent';
import Input from '../libs/components/InputComponent';
import Button from '../libs/components/ButtonComponent';
import {connect} from "react-redux";
import { Link } from "react-router-dom";

import { resetCart, removeCart, changeCart } from "../libs/actions/add-product-to-cart";

class PageOrder extends Component {
    constructor(props) {
        super(props);
        this.state = {
            values: props.cart.ids,
            name: '',
            phone: '',
            discount: '',
        };
        window.scrollTo({ top: 0, behavior: 'smooth' });
    }

    static get propTypes() {
        return {
            cart: PropTypes.shape(),
        }
    }

    static get defaultProps() {
        return {
            cart: {},
        }
    }

    clearCart = () => {
        const { dispatch } = this.props;
        dispatch(resetCart());
    };

    removeProduct = (id) => {
        const { dispatch } = this.props;
        dispatch(removeCart(id))
    };

    changeCountGoods = (product) => {
        const { dispatch } = this.props;
        dispatch(changeCart(product));
    };

    render() {
        const { values, name, phone, discount } = this.state;
        const { totalPrice } = this.props.cart;

        return (
            <HeaderSidebarMainFooter
                showMainMenu={false}
                content={
                    <div className="container_ shopping__flex">
                        <Grid
                            modified="mb-24 shopping__product_caption"
                            columns={[
                                {
                                    column: 8,
                                    content: (
                                        <p className="shopping__details-width">Детали</p>
                                    )
                                },
                                {
                                    column: 4,
                                    content: (
                                        <p className="">Цена за единицу</p>
                                    )
                                },
                                {
                                    column: 4,
                                    content: (
                                        <p className="shopping__items_value-width">Количество</p>
                                    )
                                },
                                {
                                    column: 4,
                                    content: (
                                        <p className="shopping__items_value-width">Сумма</p>
                                    )
                                },
                                {
                                    column: 4,
                                    content: (
                                        <p className="shopping__items_value-width">Действи</p>
                                    )
                                }
                            ]}
                        />
                        {values.map((item, index) => (
                            <Grid
                                key={index}
                                modified="shopping__product"
                                columns={[
                                    {
                                        column: 8,
                                        content: (
                                            <div className="shopping__details">
                                                <div className="img_block">
                                                    <img src={`${process.env.URL}/image/${item.image}`} alt=""/>
                                                </div>
                                                <div className="shopping__parametrs">
                                                    <div className="shopping__parametrs_name">{item.name}</div>
                                                </div>
                                            </div>
                                        )
                                    },
                                    {
                                        column: 4,
                                        content: (
                                            <p className="options">{item.price} &#x20BD;</p>
                                        )
                                    },
                                    {
                                        column: 4,
                                        content: (
                                            <div className="options">
                                                <Input
                                                    modifiedContainer="shopping__input-param"
                                                    value={item.count}
                                                    setValue={v => {
                                                        this.setState(prevState => {
                                                            const product = values.filter(i => i.id === item.id)[0];
                                                            product.count = v.replace(/\D+/g, '');
                                                            return { ...prevState, values }
                                                        })
                                                    }}
                                                    onValidate={() => this.changeCountGoods(item)}
                                                />
                                            </div>
                                        )
                                    },
                                    {
                                        column: 4,
                                        content: (
                                            <p className="options">{item.price*item.count} &#x20BD;</p>
                                        )
                                    },
                                    {
                                        column: 4,
                                        content: (
                                            <div
                                                className="options shopping_button-close"
                                                onClick={() => this.removeProduct(item.id)}
                                            >
                                                <i className="fas fa-times-circle"></i>
                                            </div>
                                        )
                                    },
                                ]}
                            />
                        ))}
                        {values.length === 0 && (
                            <div className="cart-empty">
                                Корзина пуста
                            </div>
                        )}
                        <div className="controls">
                            <div className="container controls__flex">
                                <Button
                                    modified="controls__button"
                                    fontModified="button-font"
                                    caption="Очистить корзину"
                                    click={this.clearCart}
                                />
                                <Link
                                    className="controls__button"
                                    to='/shop'
                                >Продолжить покупки</Link>
                            </div>
                        </div>
                        <Grid
                            modified="checkout"
                            columns={[
                                {
                                    column: 8,
                                    content: (
                                        <>
                                            <div className="caption">Данные о клиенте</div>
                                            <Input
                                                modifiedContainer="w-80 mt-10"
                                                placeholder="Имя"
                                                value={name}
                                                setValue={v => this.setState({ name: v })}
                                            />
                                            <Input
                                                modifiedContainer="w-80 mt-10"
                                                placeholder="Телефон"
                                                value={phone}
                                                setValue={v => this.setState({ phone: v })}
                                            />
                                        </>
                                    )
                                },
                                {
                                    column: 8,
                                    content: (
                                        <>
                                            <div className="caption">Купон на скидку</div>
                                            <Input
                                                modifiedContainer="w-80"
                                                placeholder="Введите 8 цифр"
                                                value={discount}
                                                setValue={v => this.setState({ discount: v.replace(/\D+/g, '').slice(0,8) })}
                                            />
                                        </>
                                    )
                                },
                                {
                                    column: 8,
                                    content: (
                                        <>
                                            <div className="details__checkout details__block-param">
                                                <div className="details__checkout_text">
                                                    <p className="details__checkout_subtotal">
                                                        Полная стоимость
                                                        <span className="details__checkout_subtotal details__checkout-margin">{totalPrice} &#x20BD;</span>
                                                    </p>
                                                    <p className="details__checkout_grandtotal">
                                                        Стоимость со скидкой
                                                        <span className="details__checkout_grandtotal details__checkout-margin details__checkout_grandtotal-color">{totalPrice} &#x20BD;</span>
                                                    </p>
                                                </div>
                                                <Button
                                                    modified="button-simple"
                                                    caption="Заказать"
                                                />
                                            </div>
                                        </>
                                    )
                                }
                            ]}
                        />
                    </div>
                }
            />
        )
    }
}

export default connect((state) => {
    const {
        cart,
        dispatch
    } = state;
    return {
        cart,
        dispatch
    };
})(PageOrder);
