import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";

import { fetchGoods } from '../libs/actions/goods';
import { fetchFilters } from '../libs/actions/filters';
import { fetchCartProduct } from "../libs/actions/cart-product";

import { addCart } from "../libs/actions/add-product-to-cart";

import HeaderSidebarMainFooter from '../libs/layout/header-sidebar-main-footer';
import Sidebar from "./components/SidebarComponent";
import Card from "./components/CardComponent";
import ModalForm from "../libs/components/ModalFormComponent";
import CartProduct from "./components/CartProductComponent";
import Grid from "../libs/components/GridComponent";

class PageShop extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showModalForm: false,
            idCard: 0,
        };
        props.dispatch(fetchFilters());
        props.dispatch(fetchGoods());
        window.scrollTo({ top: 0, behavior: 'smooth' });
    }

    static get propTypes() {
        return {
            goods: PropTypes.shape(),
            cartProduct: PropTypes.shape(),
            dispatch: PropTypes.func,
        }
    }

    static get defaultProps() {
        return {
            goods: {},
            cartProduct: {},
            dispatch: () => {},
        }
    }

    openCart = (id) => {
        const { dispatch, goods } = this.props;
        dispatch(fetchCartProduct({id_goods: id}));
        this.setState({ showModalForm: true, idCard: id });
    };

    addToCart = (id) => {
        const { dispatch, goods } = this.props;
        const product = goods.data.filter(item => item.id === id);
        dispatch(addCart(product[0]));
    };

    render() {
        const { goods, cartProduct } = this.props;
        const { showModalForm, idCard } = this.state;

        const itemGoods = goods.data?.reduce((acc,cur,index,arr) => {
            if (index % 3 === 0) return [...acc, goods.data.slice(index,index+3)];
            return [...acc];
        },[]);

        return (
            <>
                {cartProduct.data?.length > 0 && showModalForm && (
                    <ModalForm
                        content={
                            <CartProduct
                                id={idCard}
                                info={cartProduct.data[0]}
                                addToCard={this.addToCart}
                            />
                        }
                        closeForm={() => this.setState({ showModalForm: false, idCard: 0 })}
                    />
                )}
                <HeaderSidebarMainFooter
                    sidebar={<Sidebar/>}
                    content={
                        <div>
                            <main className="main">
                                {itemGoods?.map((v) => (
                                    <Grid
                                        modified="goods"
                                        columns={
                                            v.map((item) => {
                                                return {
                                                    column: 8,
                                                    content: (
                                                        <Card
                                                            key={item.id}
                                                            image_alt={""}
                                                            image_src={`${process.env.URL}/image/${item.image}`}
                                                            title={item.name}
                                                            price={item.price.toString()}
                                                            id={item.id}
                                                            openCart={this.openCart}
                                                            addToCart={this.addToCart}
                                                        />
                                                    )
                                                }
                                            })
                                        }
                                    />
                                ))}
                            </main>
                        </div>
                    }
                />
            </>
        )
    }
}

export default connect((state) => {
    const {
        goods,
        filters,
        cartProduct,
    } = state;
    return {
        goods,
        filters,
        cartProduct,
    };
})(PageShop);
