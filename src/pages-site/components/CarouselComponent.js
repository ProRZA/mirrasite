import React, { Component } from "react";
import PropTypes from "prop-types";

class Dot extends React.PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            idx: props.idx,
        }
    }

    static get propTypes() {
        return {
            active: PropTypes.string,
            idx: PropTypes.number,
            onChange: PropTypes.func,
        }
    }

    static get defaultProps() {
        return {
            active: '',
            idx: 0,
            onChange: () => {},
        }
    }

    render() {
        const { active, onChange } = this.props;
        const { idx } = this.state;

        return (
            <span
                className={`carousel_dot${active}`}
                onClick={() => onChange(idx)}
            >
            </span>
        )
    }
}

class Carousel extends Component {
    constructor(props){
        super(props);
        this.state = {
            slideIndex: 0,
            dots: [],
        };
        this.timer = (props.play ? setInterval(() => this.autoplay(),4000) : null);
    }

    static get propTypes() {
        return {
            modified: PropTypes.string,
            play: PropTypes.bool,
            slides: PropTypes.arrayOf(PropTypes.shape()),
            dispatch: PropTypes.func,
        }
    }

    static get defaultProps() {
        return {
            modified: '',
            play: false,
            slides: [],
            dispatch: () => {},
        }
    }

    componentWillUnMount() {
        clearInterval(this.timer);
        clearTimeout(this.timer);
    }

    changeDot = index => {
        this.stopTimer();
        this.setState({ slideIndex: index })
    };

    stopTimer = () => {
        clearInterval(this.timer);
        this.timer = setTimeout(() => {
            clearTimeout(this.timer);
            this.timer = setInterval(() => this.autoplay(),4000);
        },10000);
    };

    autoplay = () => {
        const { slides } = this.props;
        const { slideIndex } = this.state;

        const newSlideIndex = (slideIndex === slides.length-1) ? 0 : slideIndex + 1;
        this.setState( { slideIndex: newSlideIndex });
    };

    render() {
        const { slides, modified } = this.props;
        const { slideIndex } = this.state;

        const slider = slides[slideIndex];

        return (
            <section className="carousel">
                <div className="carousel__container">
                    {slides.map((item, index) => {
                        const fade = (index === slideIndex ? 'fade' : 'display-none');
                        return (
                            <div key={index} className={`${modified} ${fade}`}>
                                <img className="carousel__slides_image" src={item.img}/>
                                <div className="carousel__slides_text">{item.content}</div>
                            </div>
                            )
                    })}
                    <div
                        className="carousel_prev"
                        onClick={() => {
                            this.stopTimer();
                            this.setState({ slideIndex: (slideIndex === 0 ? slides.length-1 : slideIndex-1) })
                        }}
                    >&#10094;</div>

                    <div
                        className="carousel_next"
                        onClick={() => {
                            this.stopTimer();
                            this.setState({ slideIndex: (slideIndex === slides.length-1 ? 0 : slideIndex+1) })
                        }}
                    >&#10095;</div>
                    <div className="carousel__container_dot">
                        {slides.map((item, index) => {
                            const active = (index === slideIndex) ? ' active' : '';
                            return (
                                <Dot
                                    key={index}
                                    idx={index}
                                    active={active}
                                    onChange={this.changeDot}
                                />
                            )
                        })}
                    </div>
                </div>
            </section>
        );
    }
}

export default Carousel;
