import React, { Component } from "react";
import PropTypes from 'prop-types';

import { Link } from "react-router-dom";

export class MainMenu extends Component {
    constructor(props) {
        super(props);
        this.state = {
            dropdown: false,
        }
    }

    static get propTypes() {
        return {
            options: PropTypes.arrayOf(PropTypes.shape()).isRequired,
            openMiniMenu: PropTypes.bool,
        }
    }

    static get defaultProps() {
        return {
            openMiniMenu: false,
        }
    }

    clickDropDown = (e) => {
        const { dropdown } = this.state;

        const block = e.target.lastChild;
        if (block) {
            block.classList.toggle('display');
            this.setState({ dropdown: !dropdown })
        }
    };

    buildMenu = (options, deep = 0) => {
        const { openMiniMenu } = this.props;

        return (
            <ul
                className={deep > 0 ? 'dropdown' : `navigation__menu ${openMiniMenu ? 'open-menu' : ''}`}
            >
                {options.map((item, index) => {
                    if (!item.drop) {
                        return (
                            <li key={`${deep}.${index}`} className="navigation__menu-item">
                                <Link className="navigation__menu-link" to={item.links}>{item.title}</Link>
                            </li>
                        )
                    } else {
                        return (
                            <li key={`${deep}.${index}`} className="navigation__menu-item">
                                <div
                                    className="navigation__menu-link"
                                    onClick={(e) => this.clickDropDown(e)}
                                >
                                    {item.title}
                                    <i className="fa fa-caret-down arrowDown"></i>
                                    {this.buildMenu(item.links,deep+1)}
                                </div>
                            </li>
                        )
                    }
                })}
            </ul>
        );
    };

    render() {
        const { options } = this.props;

        const menu = this.buildMenu(options);

        return (
            <div className="navigation">
                <nav className="nav" role="navigation">
                    {menu}
                </nav>
            </div>
        )
    }
}

export default MainMenu;
