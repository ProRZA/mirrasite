import React, { Component } from "react";
import PropTypes from 'prop-types';
import { connect } from "react-redux";
import { Link } from "react-router-dom";

import { removeCart, addTitle } from '../../libs/actions/add-product-to-cart';

class Cart extends Component {
    constructor(props) {
        super(props);
        this.state = {
            positionMenu: {},
            showMenu: false,
        };
        this.ref = React.createRef();
    }

    static get propTypes() {
        return {
            cart: PropTypes.shape(),
            sizeWindow: PropTypes.shape(),
        }
    }

    static get defaultProps() {
        return {
            cart: {},
            sizeWindow: {},
        }
    }

    toggleMenu = () => {
        const { showMenu } = this.state;

        if (!showMenu) {
            const body = this.ref.current.getBoundingClientRect();
            const position = {
                top: body.bottom,
                left: -body.width
            };
            this.setState({ position, showMenu: true });
        } else {
            this.setState({ showMenu: false })
        }
    };

    render() {
        const { cart, dispatch, sizeWindow } = this.props;
        const { ids, title, totalPrice } = cart;
        const { position, showMenu } = this.state;

        return (
            <div
                ref={this.ref}
                className="cart__block"
                onClick={() => this.toggleMenu()}
            >
                <div className="cart-image">
                    <svg xmlns="http://www.w3.org/2000/svg" width="25" height="22" viewBox="0 0 32 29">
                        <path className="cart"
                              d="M0 1.182C0 .532.533 0 1.184 0h4.28c.533 0 1.007.354 1.145.867l4.833 17.455H24.62l4.379-10.048H14.4a1.186 1.186 0 0 1-1.184-1.182c0-.65.533-1.182 1.184-1.182h16.413c.394 0 .77.197.986.532.217.335.257.749.099 1.123l-5.405 12.412c-.198.433-.612.709-1.085.709H10.554a1.184 1.184 0 0 1-1.144-.867L4.577 2.364H1.184A1.186 1.186 0 0 1 0 1.182zM6.747 26.32a2.684 2.684 0 0 1 5.365 0 2.684 2.684 0 0 1-5.365 0zm16.788.178c-.1-1.458 1.006-2.758 2.485-2.857 1.48-.098 2.762 1.025 2.86 2.483.04.728-.177 1.418-.65 1.95a2.678 2.678 0 0 1-1.835.926h-.197c-1.401 0-2.565-1.103-2.663-2.502z"/>
                    </svg>
                </div>
                <div className="cart-total-product">
                    {title ? title : 'Корзина пуста'}
                </div>

                {showMenu && (
                    <div
                        className='cart-menu'
                        // style={position}
                    >
                        {ids.length > 0 && ids.map((item, index) => {
                            return (
                                <div key={index} className="cart-menu-section">
                                    <div className="cart-menu-link">
                                        <img src={`${process.env.URL}/image/${item.image}`} alt=""/>
                                        <div className="cart-menu-info">
                                            <p className="cart-menu-info-caption">{item.name}</p>
                                            <p className="cart-menu-info-price">{item.count} <span
                                                className="multi"> x </span> {item.price} &#x20BD;</p>
                                        </div>
                                    </div>
                                    <div
                                        className="cart-menu-iconClosed"
                                        onClick={(event) => {
                                            event.stopPropagation();
                                            event.preventDefault();
                                            dispatch(removeCart(item.id))
                                        }}
                                    >
                                        <i className="far fa-times-circle"></i>
                                    </div>
                                </div>
                            )
                        })}
                        {ids.length > 0 && (
                            <div className="commonInfo">
                                <div className="cart-menu-total_price">
                                    <p className="total_price">Всего</p>
                                    <p className="total_price">{totalPrice} &#x20BD;</p>
                                </div>
                                <div className="cart-menu-buttons">
                                    <Link to="/shop/order" className="button_cart_menu">Перейти в корзину</Link>
                                </div>
                            </div>
                        )}
                        {ids.length === 0 && (
                            <div className="cart-empty">
                                Корзина пуста
                            </div>
                        )}
                    </div>
                )}
            </div>
        )
    }
}

export default connect((state) => {
    const {
        cart,
        dispatch,
        sizeWindow,
    } = state;
    return {
        cart,
        dispatch,
        sizeWindow,
    };
})(Cart);
