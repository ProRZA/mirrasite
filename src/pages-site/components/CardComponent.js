import React, { Component } from "react";
import PropTypes from 'prop-types';

import Button from "../../libs/components/ButtonComponent";

export class Card extends Component {
    constructor(props) {
        super(props);
        this.state = {
            hover: false,
        }
    }

    static get propTypes() {
        return {
            image_src: PropTypes.string,
            image_alt: PropTypes.string,
            title: PropTypes.string,
            price: PropTypes.string,
            id: PropTypes.oneOfType([
                PropTypes.string,
                PropTypes.number,
            ]).isRequired,
            openCart: PropTypes.func,
            addToCart: PropTypes.func,
        }
    }

    static get defaultProps() {
        return {
            image_src: '',
            image_alt: '',
            title: '',
            price: '',
            openCart: () => {},
            addToCart: () => {},
        }
    }

    render() {
        const {
            image_src,
            image_alt,
            title,
            price,
            id,
            openCart,
            addToCart,
        } = this.props;
        const { hover } = this.state;

        return (
            <div
                className="product__container"
                onMouseEnter={() => this.setState({ hover: true })}
                onMouseLeave={() => this.setState({ hover: false })}
            >
                <div className="product__items">
                    <img className="product__img" src={image_src} alt={image_alt}/>
                </div>
                <div className="product__items-text">
                    <div className="product__items">
                        <p className="product__items-caption">{title}</p>
                    </div>
                    <div className="product__footer">
                        <p className="product__items-price">{price} &#x20BD;</p>
                        <Button
                            modified={"product__addToCart"}
                            caption={"В корзину"}
                            click={() => addToCart(id)}
                        />
                    </div>
                </div>
                {hover && (
                    <Button
                        modified="product__viewProduct"
                        caption={"Просмотр"}
                        click={() => openCart(id)}
                    />
                )}
            </div>
        )
    }
}

export default Card;
