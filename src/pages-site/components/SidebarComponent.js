import React, { Component } from "react";
import PropTypes from 'prop-types';
import { connect } from "react-redux";

import { fetchGoods } from '../../libs/actions/goods';

import Button from "../../libs/components/ButtonComponent";

import FilterCells from "./FilterCellsComponent";

class Sidebar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            filterSelect: [],
        };
    };

    static get propTypes() {
        return {
            filters: PropTypes.shape(),
            dispatch: PropTypes.func,
        }
    };

    static get defaultProps() {
        return {
            filters: {},
            dispatch: () => {},
        }
    };

    changeFilter = (params, checked) => {
        const { filterSelect } = this.state;
        if (checked) {
            this.setState({filterSelect: [...filterSelect, params.id]})
        } else {
            this.setState({filterSelect: filterSelect.filter(item => item !== params.id)})
        }
    };

    getGoods = () => {
        const { dispatch } = this.props;
        const { filterSelect } = this.state;

        dispatch(fetchGoods(filterSelect));
    };

    render() {
        const { filters } = this.props;

        return (
            <aside className="side-menu">
                <div className="filter">
                    <FilterCells
                        name="Тип заболевания"
                        options={filters.data?.filter(item => item.filter_name === 'illness')}
                        change={this.changeFilter}
                    />
                    <FilterCells
                        name="Для возраста"
                        options={filters.data?.filter(item => item.filter_name === 'ages')}
                        change={this.changeFilter}
                    />
                    <FilterCells
                        name="По типам кожи"
                        options={filters.data?.filter(item => item.filter_name === 'skin')}
                        change={this.changeFilter}
                    />
                    <div className="filter__caption">Цена</div>
                    <div className="filter__body">
                        <div className="price__block">

                        </div>
                    </div>
                    <div className="filter__body">
                        <Button
                            modified="button_sidebar_shop"
                            fontModified={"text-big"}
                            caption={"Фильтровать"}
                            click={() => this.getGoods()}
                        />
                    </div>
                </div>
            </aside>
        )
    }
}

export default connect((state) => {
    const {
        filters,
        dispatch,
    } = state;
    return {
        filters,
        dispatch,
    };
})(Sidebar);
