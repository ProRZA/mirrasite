import React, { Component } from "react";
import PropTypes from 'prop-types';

import Chekbox from "../../libs/components/CheckboxComponent";

class FilterCells extends Component {
    constructor(props) {
        super(props);
    }

    static get propTypes() {
        return {
            name: PropTypes.string,
            options: PropTypes.arrayOf(
                PropTypes.shape(),
           ),
            change: PropTypes.func,
        }
    }

    static get defaultProps() {
        return {
            name: '',
            change: () => {},
        }
    }

    render() {
        const {
            name,
            options,
            change,
        } = this.props;

        return (
            <>
                <div className="filter__caption">{name}</div>
                <div className="filter__body">
                {options && options.map(item => {
                        return (
                            <Chekbox
                                key={item.id}
                                modifiedContainer={"filter__body_item"}
                                labelText={item.value}
                                checkDefault={false}
                                click={change}
                                params={{id: item.id}}
                            />
                        );
                    })}
                </div>
            </>
        )
    }
}

export default FilterCells;
