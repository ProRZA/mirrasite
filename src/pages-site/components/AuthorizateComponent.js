import React, { Component } from "react";
import PropTypes from 'prop-types';

import Input from '../../libs/components/InputComponent';
import Button from '../../libs/components/ButtonComponent';
import Grid from '../../libs/components/GridComponent';

import { Link } from "react-router-dom";

class Authorizate extends Component {
    constructor(props) {
        super(props);
        this.state = {
            login: '',
            password: '',
        }
    }

    static get propTypes() {
        return {

        }
    }

    static get defaultProps() {
        return {

        }
    }

    render() {
        const { login, password } = this.state;

        return (
            <div className="authorizate_form">
                <Grid
                    modified="center-vert"
                    columns={[
                        {
                            column: 3,
                            content: (
                                <div className="text-right color-site-text">Логин</div>
                            )
                        },
                        {
                            column: 19,
                            content: (
                                <Input
                                    modifiedContainer="fields"
                                    icon="fas fa-user"
                                    value={login}
                                    setValue={v => this.setState({login: v})}
                                />
                            )
                        },
                    ]}
                />
                <Grid
                    modified="center-vert"
                    columns={[
                        {
                            column: 3,
                            content: (
                                <div className="text-right color-site-text">Пароль</div>
                            )
                        },
                        {
                            column: 19,
                            content: (
                                <Input
                                    typeInput="password"
                                    modifiedContainer="fields"
                                    icon="fas fa-key"
                                    value={password}
                                    setValue={v => this.setState({password: v})}
                                />
                            )
                        },
                    ]}
                />
                <div className="tag"></div>
                <div className="opposite-sides">
                    <Button
                        modified="button-link"
                        fontModified="cancel-uppercase"
                        caption="Забыли пароль?"
                    />
                    <Link
                        className="button_authorizated"
                        to="/lk"
                    >
                        Войти
                    </Link>
                </div>
            </div>
        )
    }
}

export default Authorizate;
