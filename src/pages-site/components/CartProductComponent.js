import React, { Component } from "react";
import PropTypes from 'prop-types';

import Button from "../../libs/components/ButtonComponent";

class CartProduct extends Component {
    constructor(props) {
        super(props);
    }

    static get propTypes() {
        return {
            id: PropTypes.oneOfType([
                PropTypes.string,
                PropTypes.number,
            ]).isRequired,
            info: PropTypes.shape(),
            addToCard: PropTypes.func,
        }
    }

    static get defaultProps() {
        return {
            info: {},
            addToCard: () => {},
        }
    }

    render() {
        const { info, addToCard, id } = this.props;

        return (
            <div className="single__product">
                <div className="single__product_block">
                    <div className="single__product_image">
                        <img
                            src={`${process.env.URL}/image/${info.image}`}
                            alt={info.alt}
                            className="image"
                        />
                            <div className="exist">Есть в наличие</div>
                    </div>
                    <div className="single__product_text">
                        <div className="">
                            <div className="single__product_text-caption">{info.name}</div>
                            <div className="single__product_text-description">
                                {info.description}
                            </div>
                        </div>
                        <div className="single__product_information">
                            <div className="single__product_text-params">
                                <div className="purpose">Для чего:</div>
                                <div
                                    className="purpose_text"
                                >
                                    {info.value}
                                </div>
                            </div>
                            <div className="buttons">
                                <div className="price">{info.price} ₽</div>
                                <Button
                                    modified="addToCart"
                                    caption="В корзину"
                                    click={() => addToCard(id)}
                                />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default CartProduct;
